Install k6s: https://k6.io/docs/getting-started/installation/
IntelliSense: https://k6.io/docs/misc/intellisense/

User Agent: `Mozilla/5.0 (Windows NT 10.0; Win64; x64)`

Running script:

- single iteration localy: `k6 run scenarios/sample.js`
- single iteration docker: `docker run -ti --rm grafana/k6 run - <scenarios/sample.js`
- debugging: `k6 run scenarios/sample.js --http-debug`
- stages: `k6 run scenarios/sample.js -s 5s:5 -s 5s:5 -s 5s:1`, where each stage is `[duration]:[target]`