import http from 'k6/http';

export const options = {
  stages: [
    { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
    { duration: '5m', target: 0 }, // ramp-down to 0 users
  ],
  noConnectionReuse: true,
  userAgent: 'User Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
};

export default function () {
  http.get("http://www.d3.ru/");
  http.get("https://d3.ru/all/new/");
  http.get("https://d3.ru/all/best/day/");
  http.get("https://d3.ru/all/pulse/");
  http.get("https://d3.ru/communities/top/");

  for (let i = 0; i < searches.length; i++) {
    const search_criteria = encodeURI(searches[i]);
    http.get(`https://d3.ru/search/?query=${search_criteria}`)
  }

  for (let i = 0; i < 50; i++) {
    http.get(`https://d3.ru/search/?query=${Math.random().toString(16).substring(2, 16)}`)
  }
}

const searches = [ "документ", "где сидит хуйло?", "бла бла бла", "билет в гаагу", "митинг", "доллар по 1000", "санкции", "пиздец всем",  ]