import http from 'k6/http';

export const options = {
  // stages: [
  //   { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
  //   { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
  //   { duration: '5m', target: 0 }, // ramp-down to 0 users
  // ],
  noConnectionReuse: true,
  userAgent: 'User Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
};

export default function () {
  http.get("https://minfin.gov.ru");
  http.get("https://minfin.gov.ru/ru/ministry/eservices/");

  for (let i = 0; i < searches.length; i++) {
    const search_criteria = encodeURI(searches[i]);
    http.get(`https://minfin.gov.ru/ru/search/?q_4=${search_criteria}`)
  }

  http.get("https://minfin.gov.ru/ru/appeal/");
}

const searches = [ "документ", "где сидит хуйло?", "бла бла бла", "билет в гаагу" ]